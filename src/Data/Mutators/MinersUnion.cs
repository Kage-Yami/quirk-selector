using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    /// <remarks>
    /// Disabled test mutator included with vanilla, with no official parameter value.
    /// </remarks>
    public record MinersUnion : Mutator
    {
        public override string LabelId => "mutator.miners_union";

        public override string Label => "Miners' union";

        public override string DescriptionId => "mutator.miners_union.desc";

        /// <remarks>
        /// Arbitrary value chosen as vanilla does not specify it.
        /// </remarks>
        public override object[] DescriptionParams => new object[] {1};


        public override string Description => "Mines on *mineral planets* require an additional upkeep of {1}:$:.";

        public override MutatorKind Kind => MutatorKind.Negative;

        public override bool Hidden => false;

        public override int? ScoreModifier => 5;

        public override List<string>? Packages => null;

        public override List<string>? Conditions => null;

        public override List<string> Effects => new() {"quality(MinersUnionQuality(1))"};
    }
}
