using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record SkilledScientists : Mutator
    {
        public override string LabelId => "mutator.skilled_scientists";

        public override string Label => "Skilled scientists";

        public override string DescriptionId => "mutator.skilled_scientists.desc";

        public override object[] DescriptionParams => new object[] {1};


        public override string Description => "Labs receiving at least two :P: get [[delta:{1}S]].";

        public override MutatorKind Kind => MutatorKind.Positive;

        public override bool Hidden => false;

        public override int? ScoreModifier => -15;

        public override List<string>? Packages => null;

        public override List<string>? Conditions => null;

        public override List<string> Effects => new() {"quality(SkilledScientistsQuality())"};
    }
}
