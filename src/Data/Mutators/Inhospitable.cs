using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record Inhospitable : Mutator
    {
        public override string LabelId => "mutator.no_earth";

        public override string Label => "Inhospitable";

        public override string DescriptionId => "mutator.no_planet_of_one_type.desc";

        public override object[] DescriptionParams => new object[] {"planet.earthlike"};


        public override string Description => "This sector contains no [[ref:{1}]] planets.";

        public override MutatorKind Kind => MutatorKind.Composition;

        public override bool Hidden => false;

        public override int? ScoreModifier => 5;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MapGenNoEarth()"};

        public override List<string>? Effects => null;
    }
}
