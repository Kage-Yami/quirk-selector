using System;

namespace QuirkSelector.Data
{
    public enum MutatorKind
    {
        Composition,
        Improvement,
        Limitation,
        Negative,
        Positive,
        Quirk,
        ScoreReplacing,
        SectorType
    }

    public static class MutatorKindExtensions
    {
        public static string AsString(this MutatorKind kind) => kind switch {
            MutatorKind.Composition => "composition",
            MutatorKind.Improvement => "improvement",
            MutatorKind.Limitation => "limitation",
            MutatorKind.Negative => "negative",
            MutatorKind.Positive => "positive",
            MutatorKind.Quirk => "quirk",
            MutatorKind.ScoreReplacing => "score_replacing",
            MutatorKind.SectorType => "sector_type",
            _ => throw new ArgumentOutOfRangeException(nameof(kind), kind, null)
        };
    }
}
