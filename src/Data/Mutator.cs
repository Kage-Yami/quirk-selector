using System.Collections.Generic;
using System.Linq;
using IronPython.Runtime;
using QuirkSelector.Data.Mutators;
using Slipways.General.Localizations;
using Slipways.Main.Empires.Mutators;

namespace QuirkSelector.Data
{
    public abstract record Mutator
    {
        // PROPERTIES

        /// <remarks>
        /// Must be unique across all variants (both vanilla and modded); this property is used for the record's hash code.
        /// </remarks>
        public abstract string LabelId { get; }

        public abstract string Label { get; }

        public abstract string DescriptionId { get; }
        public abstract object[]? DescriptionParams { get; }
        public abstract string Description { get; }

        public abstract MutatorKind Kind { get; }
        public abstract bool Hidden { get; }

        public abstract int? ScoreModifier { get; }

        public abstract List<string>? Packages { get; }
        public abstract List<string>? Conditions { get; }
        public abstract List<string>? Effects { get; }

        // CALCULATED FIELDS

        private LocalizedString? _localisedDescription;

        public LocalizedString LocalisedDescription() =>
            _localisedDescription ??=
                null == DescriptionParams
                    ? new(DescriptionId, Description)
                    : new(DescriptionId, Description, DescriptionParams);

        // OVERRIDES

        public override int GetHashCode()
        {
            return LabelId.GetHashCode();
        }

        // CASTS

        public static implicit operator MutatorSpec(Mutator mutator)
        {
            PythonDictionary dict = new() {
                { "label", new LocalizedString(mutator.LabelId, mutator.Label) },
                { "description", mutator.LocalisedDescription() },
                { "kind", mutator.Kind.AsString() },
                { "hidden", mutator.Hidden }
            };

            if (null != mutator.ScoreModifier) {
                dict.Add("score_modifier", mutator.ScoreModifier);
            }

            if (null != mutator.Packages) {
                dict.Add("packages", mutator.Packages);
            }

            if (null != mutator.Conditions) {
                dict.Add("conditions", mutator.Conditions);
            }

            if (null != mutator.Effects) {
                dict.Add("effects", mutator.Effects);
            }

            return MutatorSpec.From(dict);
        }

        public static implicit operator Mutator(MutatorSpec spec) => InitialisedMutators.First(mutator => mutator.LabelId == spec.Label.ID);

        // METHODS

        /// <summary>
        /// Should the mutator be disabled due to other conflicting mutators already being enabled?
        /// </summary>
        /// <returns><c>true</c> if any conflicts are selected, otherwise <c>false</c></returns>
        public bool AreConflictsSelected(IDictionary<Mutator, bool> mutatorSelections) =>
            ConflictingMutators.ContainsKey(this)
            && ConflictingMutators[this].Any(potentialConflicts => mutatorSelections[potentialConflicts]);

        // STATICS

        // Compositions
        public static readonly Mutator DrySand = new DrySand();
        public static readonly Mutator FaintSuns = new FaintSuns();
        public static readonly Mutator FormerIndustrialZone = new FormerIndustrialZone();
        public static readonly Mutator Hospitable = new Hospitable();
        public static readonly Mutator Inhospitable = new Inhospitable();
        public static readonly Mutator TeemingWithLife = new TeemingWithLife();
        public static readonly Mutator YoungPlanets = new YoungPlanets();

        // Improvements
        public static readonly Mutator HighBirthRate = new HighBirthRate();

        // Limitations
        public static readonly Mutator DecrepitRuins = new DecrepitRuins();

        // Negatives
        public static readonly Mutator MinersUnion = new MinersUnion();
        public static readonly Mutator WeakSlipspace = new WeakSlipspace();

        // Positives
        public static readonly Mutator RichDeposits = new RichDeposits();
        public static readonly Mutator SkilledScientists = new SkilledScientists();

        // Quirks
        public static readonly Mutator AbundantResources = new AbundantResources();
        public static readonly Mutator BeautifulSights = new BeautifulSights();
        public static readonly Mutator HarmfulRadiation = new HarmfulRadiation();
        public static readonly Mutator UnstablePlanets = new UnstablePlanets();

        // Scoring focuses
        public static readonly Mutator ExportFocus = new ExportFocus();
        public static readonly Mutator IndustryFocus = new IndustryFocus();
        public static readonly Mutator MiningFocus = new MiningFocus();
        public static readonly Mutator PopulationFocus = new PopulationFocus();
        public static readonly Mutator ProjectFocus = new ProjectFocus();
        public static readonly Mutator TechnologyFocus = new TechnologyFocus();

        // Sector types
        public static readonly Mutator AnomalousSector = new AnomalousSector();
        public static readonly Mutator IzziumSector = new IzziumSector();

        private static readonly HashSet<Mutator> InitialisedMutators = new() {
            DrySand, FaintSuns, FormerIndustrialZone, Hospitable, Inhospitable, TeemingWithLife, YoungPlanets,
            HighBirthRate,
            DecrepitRuins,
            MinersUnion, WeakSlipspace,
            RichDeposits, SkilledScientists,
            AbundantResources, BeautifulSights, HarmfulRadiation, UnstablePlanets,
            ExportFocus, IndustryFocus, MiningFocus, PopulationFocus, ProjectFocus, TechnologyFocus,
            AnomalousSector, IzziumSector
        };

        /// <summary>
        /// Get a collection of all the mutator variants that have been registered.
        /// </summary>
        /// <returns>Registered mutator variants</returns>
        public static HashSet<Mutator> Variants() => InitialisedMutators;

        /// <summary>
        /// Register a new mutator variant to be available for selection.
        /// </summary>
        /// <param name="variant">Mutator to register</param>
        /// <remarks>Intended for other mods that might want to add their own custom Mutators (not used internally).</remarks>
        public static void RegisterVariant(Mutator variant) => InitialisedMutators.Add(variant);

        /// <summary>
        /// Register multiple new mutator variants to be available for selection.
        /// </summary>
        /// <param name="variants">Collection of mutators to register</param>
        /// <remarks>Intended for other mods that might want to add their own custom Mutators (not used internally).</remarks>
        public static void RegisterVariants(IEnumerable<Mutator> variants)
        {
            foreach (Mutator variant in variants) {
                InitialisedMutators.Add(variant);
            }
        }

        private static readonly Dictionary<Mutator, HashSet<Mutator>> ConflictingMutators = new() {
            { AnomalousSector, new() { SkilledScientists } },
            { ExportFocus, new() { IndustryFocus, MiningFocus, PopulationFocus, ProjectFocus, TechnologyFocus } },
            { Hospitable, new() { Inhospitable } },
            { IndustryFocus, new() { ExportFocus, MiningFocus, PopulationFocus, ProjectFocus, TechnologyFocus } },
            { Inhospitable, new() { Hospitable } },
            { MiningFocus, new() { ExportFocus, IndustryFocus, PopulationFocus, ProjectFocus, TechnologyFocus } },
            { PopulationFocus, new() { ExportFocus, IndustryFocus, MiningFocus, ProjectFocus, TechnologyFocus } },
            { ProjectFocus, new() { ExportFocus, IndustryFocus, MiningFocus, PopulationFocus, TechnologyFocus } },
            { SkilledScientists, new() { AnomalousSector } },
            { TechnologyFocus, new() { ExportFocus, IndustryFocus, MiningFocus, PopulationFocus, ProjectFocus } }
        };

        /// <summary>
        /// Get a mapping of mutators to any other conflicting mutators.
        /// </summary>
        /// <returns>Map of conflicting mutators</returns>
        public static Dictionary<Mutator, HashSet<Mutator>> Conflicts() => ConflictingMutators;

        /// <summary>
        /// Register a new conflicting mutator pair that needs to be handled during selection.
        /// </summary>
        /// <param name="a">First mutator in conflicting pair</param>
        /// <param name="b">Second mutator in conflicting pair</param>
        /// <remarks>Intended for other mods that might want to add their own custom Mutators (not used internally).</remarks>
        public static void RegisterConflict(Mutator a, Mutator b)
        {
            if (ConflictingMutators.ContainsKey(a)) {
                ConflictingMutators[a].Add(b);
            } else {
                ConflictingMutators.Add(a, new() { b });
            }

            if (ConflictingMutators.ContainsKey(b)) {
                ConflictingMutators[b].Add(a);
            } else {
                ConflictingMutators.Add(b, new() { a });
            }
        }

        /// <summary>
        /// Initialise a dictionary with all possible mutators as the keys, and the default of specified type as each value.
        /// </summary>
        /// <typeparam name="TValue">Type for initialising default values</typeparam>
        /// <returns>Dictionary of mutators to values</returns>
        public static Dictionary<Mutator, TValue?> InitialiseDictionary<TValue>() => InitialisedMutators.ToDictionary(
            variant => variant,
            _ => default(TValue)
        );
    }
}
