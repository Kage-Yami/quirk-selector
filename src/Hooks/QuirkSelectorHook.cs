using HarmonyLib;
using IronPython.Runtime;
using QuirkSelector.Data;
using Slipways.General.GameData.Seeds;
using Slipways.Menus.TechnicalPages;
using Slipways.Toplevel.Preps;

namespace QuirkSelector.Hooks
{
    internal static class QuirkSelectorHook
    {
        /// <summary>
        /// Called after opening the sector selection screen.
        /// </summary>
        /// <remarks>
        /// Used to open the quirk selector window for standard and endless game modes.
        /// </remarks>
        [HarmonyPatch(typeof(PrepViews), nameof(PrepViews.ShowSeedMap), typeof(PythonDictionary))]
        [HarmonyPostfix]
        private static void OnStartSectorSelection()
        {
            if (Slipways.GameConfig().GameMode.AsGameMode() is GameMode.Standard or GameMode.Endless) {
                QuirkSelector.DisplayQuirkSelector = true;
                QuirkSelector.ResetQuirkSelection = true;
            }
        }

        /// <summary>
        /// Called after a new sector is generated on the sector selection screen.
        /// </summary>
        /// <remarks>
        /// Used to reset the selected quirks in the quirk selector window.
        /// </remarks>
        [HarmonyPatch(typeof(SectorSpec), MethodType.Constructor, typeof(PythonDictionary), typeof(bool))]
        [HarmonyPostfix]
        private static void OnSectorSelectionChange()
        {
            if (QuirkSelector.DisplayQuirkSelector) {
                QuirkSelector.ResetQuirkSelection = true;
            }
        }

        /// <summary>
        /// Called after closing the sector selection screen:
        /// <list type="bullet">
        ///     <item>Going back to the main menu</item>
        ///     <item>Moving on to the council selection screen</item>
        /// </list>
        /// </summary>
        /// <remarks>
        /// Used to close the quirk selector window.
        /// </remarks>
        [HarmonyPatch(typeof(PageScriptedFlow), nameof(PageScriptedFlow.BackOut))]
        [HarmonyPatch(typeof(PrepViews), nameof(PrepViews.ShowRaceSelect), typeof(PythonDictionary))]
        [HarmonyPostfix]
        private static void OnStopSectorSelection()
        {
            QuirkSelector.DisplayQuirkSelector = false;
        }
    }
}
